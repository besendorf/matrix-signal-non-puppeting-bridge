# Matrix/Signal Non-puppeting bridge
A matrix-signal bridge that doesn't require your own a matrix Homeserver, nor an phone running signal, though you must be able to receive SMS.

You can run this on a raspberry pi or another server and let it relay messages from matrix to signal and vice-versa.

Inspiration taken and examples followed from:
- [matrix-discord-bridge non-puppeting](https://github.com/git-bruh/matrix-discord-bridge/tree/main/bridge)
- [matrix-nio examples](https://matrix-nio.readthedocs.io/en/latest/examples.html#a-basic-client)
- [matrix-commander](https://github.com/8go/matrix-commander)

# Installation

## Requirements
```
python3.10
pipenv
supervisor
docker
docker-compose
signal-captcha-helper
```
`signal-captcha-helper` can be downloaded from https://gitlab.com/signald/captcha-helper.

### Alternative requirements (Beta)
```
nix
signaldctl
signal-captcha-helper
```

`signaldctl` can be installed from: https://signald.org/signaldctl/.
`signal-captcha-helper` can be downloaded from https://gitlab.com/signald/captcha-helper.

See the [alternative step-by-step](#Alternative\ step-by-step) for further installation instructions

### Alpine extra packages
```
apk add --no-cache \
    make \
    cmake \
    gcc \
    g++ \
    git \
    libffi-dev \
    python3-dev \
    libstdc++ \
    py3-olm \
    olm-dev
```

## Step-by-step
1. Clone this repo and enter the folder in your terminal.
2. Run `docker-compose up` and wait for signald to show that it is ready.
3. In a separate terminal, run `pipenv install`.
4. Run `docker run -it -v $PWD/signald:/signald ghcr.io/heptalium/signaldctl signaldctl account register +1234567890`, replacing with your real phone number. If you are required to give a captcha, run `signal-captcha-helper` and copy the output from the terminal. Rerun the above docker command with the captcha output: `docker run -it -v $PWD/signald:/signald ghcr.io/heptalium/signaldctl signaldctl account register +1234567890 -c <captcha_output>`.
5. You should receive an SMS with a code. Run `docker run -it -v $PWD/signald:/signald ghcr.io/heptalium/signaldctl signaldctl account verify +1234567890 <code>`.
6. Run `pipenv run python src/matrix_listen.py --log info` and follow the instructions on screen.
7. Add any chat connections that you know the signal ID/phone number and matching matrix room ID of to `config/config.json`. Any chat that you don't know the UUID/number of will be automatically mapped when a message is received (on the signal side).
7. Stop docker-compose in your first terminal, then rerun with `docker-compose up -d`.
8. Start supervisor with: `supervisord -c bridge-supervisor.conf`. Done!


### Alternative step-by-step (Beta)
1. Clone this repo and enter the folder in your terminal.
2. Run `./install.sh` and follow the on-screen instructions.
3. Run `signaldctl --socket signald/signald.sock account register +1234567890`, replacing with your real phone number. If you are required to give a captcha, run `signal-captcha-helper` and copy the output from the terminal. Rerun the above docker command with the captcha output: `signaldctl --socket signald/signald.sock account register +1234567890 -c <captcha_output>`.
4. You should receive an SMS with a code. Run `signaldctl --socket signald/signald.sock account verify +1234567890 <code>`.
5. Add any chat connections that you know the signal ID/phone number and matching matrix room ID of to `config/config.json`. Any chat that you don't know the UUID/number of will be automatically mapped when a message is received (on the signal side).
6. Run `./run.sh`. Done!


# To do
- Dockerize everything
- Use a single python script with actual multithreading instead of two python scripts run by supervisor.
- Make it extensible for other chat protocols?
- Add tests
- Fix `pipenv` non-resolvability
