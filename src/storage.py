import json
import logging
import time
from dataclasses import asdict
from importlib import import_module
from typing import Optional, List

import sqlite3
from nio import MegolmEvent

latest_db_version = 0

class Storage(object):
    def __init__(self, db_path):
        """Set up the database
        Runs an initial setup or migrations depending on whether a database file has already
        been created
        Args:
            db_path (str): The name of the database file
        """
        self.db_path = db_path

        self._initial_setup()
        self._run_migrations()

    def _initial_setup(self):
        """Initial setup of the database"""
        logging.info("Performing initial database setup...")

        # Initialize a connection to the database
        self.conn = sqlite3.connect(self.db_path)
        self.conn.row_factory = sqlite3.Row
        self.cursor = self.conn.cursor()

        # Create database_version table if it doesn't exist
        try:
            self.cursor.execute("""
                CREATE TABLE database_version (version INTEGER)
            """)
            self.cursor.execute("""
                insert into database_version (version) values (0)
            """)
            self.cursor.execute("""
                CREATE TABLE encrypted_events (
                id INTEGER PRIMARY KEY autoincrement,
                device_id text,
                event_id text unique,
                room_id text,
                session_id text,
                event text
               )
            """)
            self.cursor.execute("""
                CREATE INDEX encrypted_events_session_id_idx on encrypted_events (session_id);
            """)
            self.conn.commit()
        except sqlite3.OperationalError:
            pass

        logging.info("Database setup complete")

    def _run_migrations(self):
        """Execute database migrations"""
        # Get current version of db
        results = self.cursor.execute("select version from database_version")
        version = results.fetchone()[0]
        if version >= latest_db_version:
            logging.info("No migrations to run")
            return

        while version < latest_db_version:
            version += 1
            version_string = str(version).rjust(3, "0")
            migration = import_module(f"migrations.{version_string}")
            migration.forward(self.cursor)
            logging.info(f"Executing database migration {version_string}")
            self.cursor.execute("update database_version set version = ?", (version_string,))
            self.conn.commit()
            logging.info(f"...done")

    def get_encrypted_events(self, session_id: str):
        results = self.cursor.execute("""
            select * from encrypted_events where session_id = ?;
        """, (session_id,))
        return results.fetchall()

    def remove_encrypted_event(self, event_id: str):
        self.cursor.execute("""
            delete from encrypted_events where event_id = ?;
        """, (event_id,))
        self.conn.commit()

    def store_encrypted_event(self, event: MegolmEvent):
        try:
            event_dict = asdict(event)
            event_json = json.dumps(event_dict)
            self.cursor.execute("""
                insert into encrypted_events
                    (device_id, event_id, room_id, session_id, event) values
                    (?, ?, ?, ?, ?)
            """, (event.device_id, event.event_id, event.room_id, event.session_id, event_json))
            self.conn.commit()
        except Exception as ex:
            logging.error("Failed to store encrypted event %s: %s" % (event.event_id, ex))
