import asyncio
import argparse
import logging
from bridge import Bridge
from signal import signal, SIGUSR1

parser = argparse.ArgumentParser()
parser.add_argument(
    "-log",
    "--loglevel",
    default="warning",
    help="Provide logging level. Example --loglevel debug, default=warning",
)

args = parser.parse_args()
logging.basicConfig(level=args.loglevel.upper())
bridge = None


async def main():
    def reload(signal_received, frame):
        logging.info("Received SIGUSR1, reloading config from disk.")
        bridge.load_config_from_disk()

    signal(SIGUSR1, reload)
    logging.info("Creating Bridge class for signal.")
    bridge = Bridge()
    await bridge.initialize_matrix()
    await bridge.main_matrix()


asyncio.run(main())
