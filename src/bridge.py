import json
import os
import sys
import getpass
import logging
from typing import Optional
from nio import (
    AsyncClient,
    ClientConfig,
    MatrixRoom,
    RoomMessageText,
    LoginResponse,
    RoomCreateResponse,
    RoomKeyEvent,
    ForwardedRoomKeyEvent,
    MegolmEvent,
)
from signald import Signal

from storage import Storage

DATA_PATH = "./data/"
STATE_FILE = "state.txt"
CONFIG_FILE = "./config/config.json"


class Bridge:
    def __init__(self) -> None:
        self.matrix = None
        self.signal = None

        if os.path.exists(CONFIG_FILE):
            logging.info("Config file exists.")
            self.load_config_from_disk()

            logging.info("Starting signal client.")
            self.signal = Signal(
                self.config["signal"]["number"], self.config["signal"]["socket"]
            )

    def load_config_from_disk(self) -> None:
        with open(CONFIG_FILE, "r", encoding="UTF-8") as file:
            self.config = json.loads(file.read())

    def send_to_signal(self, message: str, recipient: str):
        logging.info("Sending '%s' to %s", message, recipient)
        if recipient.startswith("+"):
            try:
                self.signal.send(recipient=recipient, text=message)
            except:
                logging.error("Can't send '%s' to %s.", message, recipient)
        else:
            try:
                self.signal.send(recipient_group_id=recipient, text=message)
            except:
                logging.info("Failed to send.")

    async def send_to_matrix(self, message: str, room: str) -> None:
        logging.info("Sending '%s' to %s", message, room)
        content = self.matrix.encrypt({"msgtype": "m.text", "body": message})
        await self.matrix.room_send(
            room_id=room,
            message_type="m.room.message",
            content=content,
        )

    def get_signal_recipient(self, room: MatrixRoom) -> Optional[str]:
        logging.info("Finding signal recipient matching %s", room.room_id)
        self.load_config_from_disk()
        recipient_id = next(
            (
                recipient_id
                for recipient_id, room_id in self.config["bridged_rooms"].items()
                if room_id == room.room_id
            ),
            None,
        )

        if recipient_id is None:
            logging.warning("Couldn't match %s to signal recipient.", room.room_id)

        return recipient_id

    async def create_new_bridged_room(self, signal_sender: str) -> Optional[str]:
        room_response = await self.matrix.room_create(
            name=signal_sender,
            invite=self.config["matrix"]["default_invitees"],
            topic="Room automatically created by matrix-signal bridge.",
            initial_state=[
                {
                "type": "m.room.encryption",
                "state_key": "",
                "content": {
                    "algorithm": "m.megolm.v1.aes-sha2",
                    "rotation_period_ms": 604800000,
                    "rotation_period_msgs": 100,
                    }
                }]
            # power_level_override={
            # "users": {
            # invitee: 100 for invitee in self.config["matrix"]["default_invitees"]
            # }.update({
            # self.config['matrix']['user_id']: 100
            # })
            # },
        )

        if isinstance(room_response, RoomCreateResponse):
            logging.info("New bridged room created.")
            self.config["bridged_rooms"][signal_sender] = room_response.room_id
            logging.info("Writing new bridged room to config.")
            self.write_to_disk(self.config)
            return room_response.room_id

        # Otherwise the reponse is an error
        logging.error("Failed to create room with error: %s", room_response.message)

    async def get_matrix_room_from_signal_message(self, signal_message) -> str:
        # Get the signal sender from the group or if not a group, use the number directly.
        self.load_config_from_disk()
        signal_sender = signal_message.group_v2.get(
            "id", signal_message.source["number"]
        )
        logging.info("Finding matrix room matching %s.", signal_sender)

        matrix_room = self.config["bridged_rooms"].get(signal_sender)

        if matrix_room is None:
            logging.warning(
                "Message received from unmapped signal recipient: '%s'. \
                New bridged room will be created.",
                signal_sender,
            )
            matrix_room = await self.create_new_bridged_room(signal_sender)

        return matrix_room

    async def receive_signal_message(self, message) -> None:
        logging.info("Received message on signal.")
        if message.text is not None:
            matrix_room = await self.get_matrix_room_from_signal_message(message)

            logging.info("Forwarding message to matrix.")
            await self.send_to_matrix(
                f"{message.source['number']}: {message.text}", matrix_room
            )
        else:
            logging.warning("Received unimplemented signal message/event.")
            logging.info(message)

    async def receive_matrix_message(
        self, room: MatrixRoom, event: RoomMessageText
    ) -> None:
        logging.info(
            "Received message on matrix (%s) from %s.", room.room_id, event.sender
        )
        if event.sender != self.config["matrix"]["user_id"]:
            signal_recipient = self.get_signal_recipient(room)
            if signal_recipient != "*" and signal_recipient is not None:
                logging.info("Forwarding message to signal (%s).", signal_recipient)
                self.send_to_signal(recipient=signal_recipient, message=event.body)

    @staticmethod
    def write_to_disk(data: dict) -> None:
        with open(CONFIG_FILE, "w", encoding="UTF-8") as file:
            # write the login details to disk
            json.dump(
                data,
                file,
                indent=4,
            )

    def create_initial_config(
        self, resp: LoginResponse, homeserver: str, signal_number: str
    ) -> None:
        """Writes the required login details to disk so we can log in later without
        using a password.

        """
        # open the config file in write-mode
        self.write_to_disk(
            {
                "matrix": {
                    "homeserver": homeserver,
                    "user_id": resp.user_id,
                    "device_id": resp.device_id,
                    "access_token": resp.access_token,
                    "default_invitees": [],
                },
                "signal": {
                    "number": signal_number,
                    "socket": "./signald/signald.sock",
                },
                "bridged_rooms": {
                    "signal_recipient_id": "matrix_room_id",
                    "*": "default_fallback_matrix_room",
                },
            },
        )

    async def run_interactive_login(self):
        print(
            "First time use. Did not find credential file. Asking for "
            "homeserver, user, and password to create credential file."
        )
        signal_number = "+1234567890"
        signal_number = input(f"Enter your signal phone number: [{signal_number}] ")

        homeserver = "https://matrix.example.org"
        homeserver = input(f"Enter your homeserver URL: [{homeserver}] ")

        if not (homeserver.startswith("https://") or homeserver.startswith("http://")):
            homeserver = "https://" + homeserver

        user_id = "@user:example.org"
        user_id = input(f"Enter your full user ID: [{user_id}] ")

        device_name = "matrix-nio"
        device_name = input(f"Choose a name for this device: [{device_name}] ")

        client_config = ClientConfig(
            store_sync_tokens=True,
            store_name=STATE_FILE,
            encryption_enabled=True)
        client = AsyncClient(
            homeserver=homeserver,
            user=user_id,
            store_path=DATA_PATH,
            config=client_config,
            encryption_enabled=True,
        )
        password = getpass.getpass()

        resp = await client.login(password, device_name=device_name)

        # check that we logged in succesfully
        if isinstance(resp, LoginResponse):
            self.create_initial_config(resp, homeserver, signal_number)
        else:
            print(f'homeserver = "{homeserver}"; user = "{user_id}"')
            print(f"Failed to log in: {resp}")
            sys.exit(1)

        print(
            f"Add your bridged chats to the config file at {CONFIG_FILE}",
        )
        await client.close()
        sys.exit(0)

    def log_in_with_stored_config(self):
        client_config = ClientConfig(store_sync_tokens=True, store_name=STATE_FILE)
        client = AsyncClient(
            homeserver=self.config["matrix"]["homeserver"],
            store_path=DATA_PATH,
            config=client_config,
            user=self.config["matrix"]["user_id"],
            device_id=self.config["matrix"]["device_id"],
        )
        client.access_token = self.config["matrix"]["access_token"]
        return client

    async def check_login_status(self) -> AsyncClient:
        # If there are no previously-saved config, we'll use the password
        if not os.path.exists(CONFIG_FILE):
            logging.info("Running interactive login.")
            return await self.run_interactive_login()

        # Otherwise the config file exists, so we'll use the stored config
        logging.info("Logging in with stored credentials.")
        return self.log_in_with_stored_config()

    async def room_key(self, event: RoomKeyEvent):
        """Callback for ToDevice events like room key events."""
        events = self.store.get_encrypted_events(event.session_id)
        logging.debug("Got room key event for session %s, matched sessions: %s" % (event.session_id, len(events)))
        if not events:
            return

        for encrypted_event in events:
            try:
                event_dict = json.loads(encrypted_event["event"])
                params = event_dict["source"]
                params["room_id"] = event_dict["room_id"]
                params["transaction_id"] = event_dict["transaction_id"]
                megolm_event = MegolmEvent.from_dict(params)
            except Exception as ex:
                logging.warning("Failed to restore MegolmEvent for %s: %s" % (encrypted_event["event_id"], ex))
                continue
            try:
                decrypted = self.matrix.decrypt_event(megolm_event)
            except Exception as ex:
                logger.warning(f"Error decrypting event %s: %s" % (megolm_event.event_id, ex))
                continue
            if isinstance(decrypted, Event):
                logging.info(f"Successfully decrypted stored event %s" % decrypted.event_id)
                parsed_event = Event.parse_event(decrypted.source)
                logging.info(f"Parsed event: %s" % parsed_event)
                self.store.remove_encrypted_event(decrypted.event_id)
                await self.receive_matrix_message(encrypted_event["room_id"], parsed_event)
            else:
                logger.warning(f"Failed to decrypt event %s" % (decrypted.event_id,))

    async def main_matrix(self) -> None:
        try:
            logging.info("Adding message callback.")
            self.matrix.add_event_callback(self.receive_matrix_message, RoomMessageText)
            self.matrix.add_to_device_callback(self.room_key, (ForwardedRoomKeyEvent, RoomKeyEvent))
            logging.info("Listening for matrix messages.")
            
              # Sync encryption keys with the server
              # Required for participating in encrypted rooms
            if self.matrix.should_upload_keys:
                await self.matrix.keys_upload()
            await self.matrix.sync_forever(timeout=30000, full_state=True)
        finally:
            await self.matrix.close()

    async def main_signal(self):
        logging.info("Checking signal client.")
        if self.signal is not None:
            logging.info("Signal client is available, listening for messages.")
            for message in self.signal.receive_messages():
                await self.receive_signal_message(message)

    async def initialize_matrix(self) -> None:
        logging.info(
            "Checking matrix login status and potentially running interactive login."
        )
        self.matrix = await self.check_login_status()
        self.store = Storage(DATA_PATH + "db.sqlite")
        logging.info("Logged in to matrix.")
