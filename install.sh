#!/usr/bin/env nix-shell
#!nix-shell -i bash shell.nix

if (! pgrep -f signald); then
    supervisord -c signald-supervisor.conf
fi

pipenv install
pipenv run python src/matrix_listen.py --log info
