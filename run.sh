#!/usr/bin/env nix-shell
#!nix-shell -i bash shell.nix

if (! pgrep -f signald); then
    supervisord -c signald-supervisor.conf
fi

supervisord -c bridge-supervisor.conf
